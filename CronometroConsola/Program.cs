﻿namespace CronometroConsola
{
    using System;
    using ExercicioCronometro;
    class Program
    {
        static void Main(string[] args)
        {
            var relogio = new Cronometro();
            Console.WriteLine("Pressiona Enter para iniciar o cronometro");
            Console.ReadLine();

            relogio.StartClock();
            Console.WriteLine("Pressiona Enter novamente para parar o cronometro");

            while (relogio.ClockState())
            {
                var tempo = DateTime.Now - relogio.StartTime();
                Console.Write("\r Tempo Corrente: {0}", tempo);

                if (Console.KeyAvailable)
                {
                    if (Console.ReadKey().Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }
            }
            relogio.StopClock();

            Console.WriteLine("\r Tempo Cronomerado: {0}",relogio.GeTimeSpan());
            Console.ReadKey();
        }
    }
}
