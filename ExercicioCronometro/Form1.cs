﻿namespace ExercicioCronometro
{
    using System;
    using System.Windows.Forms;
    public partial class Form1 : Form
    {
        private readonly Cronometro _cronometro;
        public Form1()
        {
            InitializeComponent();
            _cronometro=new Cronometro();
            
        }

        private void Btn_OnOff_Click(object sender, EventArgs e)
        {
            if (_cronometro.ClockState())
            {
                _cronometro.StopClock();
                Btn_OnOff.Text = "Liga";
                // Lbl_Contado.Text = _cronometro.GeTimeSpan().ToString();
                TimerRelogio.Enabled = false;
            }
            else
            {
                _cronometro.StartClock();
                Btn_OnOff.Text = "Desliga";
                TimerRelogio.Enabled=true;
            }
        }

        private void TimerRelogio_Tick(object sender, EventArgs e)
        {
           UpdateLabel();
        }

        private void UpdateLabel()
        {
            var tempo = DateTime.Now - _cronometro.StartTime();
            Lbl_Contado.Text = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D3}", tempo.Hours, tempo.Minutes, tempo.Seconds,
                tempo.Milliseconds);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
