﻿namespace ExercicioCronometro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Btn_OnOff = new System.Windows.Forms.Button();
            this.Lbl_Contado = new System.Windows.Forms.Label();
            this.TimerRelogio = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // Btn_OnOff
            // 
            this.Btn_OnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_OnOff.Location = new System.Drawing.Point(409, 265);
            this.Btn_OnOff.Name = "Btn_OnOff";
            this.Btn_OnOff.Size = new System.Drawing.Size(99, 66);
            this.Btn_OnOff.TabIndex = 0;
            this.Btn_OnOff.Text = "Ligar";
            this.Btn_OnOff.UseVisualStyleBackColor = true;
            this.Btn_OnOff.Click += new System.EventHandler(this.Btn_OnOff_Click);
            // 
            // Lbl_Contado
            // 
            this.Lbl_Contado.AutoSize = true;
            this.Lbl_Contado.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Contado.Location = new System.Drawing.Point(205, 155);
            this.Lbl_Contado.Name = "Lbl_Contado";
            this.Lbl_Contado.Size = new System.Drawing.Size(150, 25);
            this.Lbl_Contado.TabIndex = 1;
            this.Lbl_Contado.Text = "00:00:00:000";
            // 
            // TimerRelogio
            // 
            this.TimerRelogio.Interval = 30;
            this.TimerRelogio.Tick += new System.EventHandler(this.TimerRelogio_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 375);
            this.Controls.Add(this.Lbl_Contado);
            this.Controls.Add(this.Btn_OnOff);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_OnOff;
        private System.Windows.Forms.Label Lbl_Contado;
        private System.Windows.Forms.Timer TimerRelogio;
    }
}

